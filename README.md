# RP-chat module


## API

```js
import RPChats from 'rp-chat';

const rpChats = new RPChats(config);

rpChats.onChats((chats: Chat[]) => {});
rpChats.onChatsNew((chat: Chat) => {});
rpChats.onChatsEdit((chat: Chat) => {});
rpChats.onChatsDelete((chat: Chat) => {});

rpChats.onChannels((channels: Channel[]) => {});
rpChats.onChannelsNew((channel: Channel) => {});
rpChats.onChannelsEdit((channel: Channel) => {});
rpChats.onChannelsDelete((channel: Channel) => {});

rpChats.onMessages((messages: Message[]) => {});
rpChats.onMessagesNew((message: Message) => {});
rpChats.onMessagesEdit((message: Message) => {});
rpChats.onMessagesDelete((message: Message) => {});

rpChats.chats();
rpChats.chatsNew();
rpChats.chatsEdit();
rpChats.chatsDelete();

rpChats.channels();
rpChats.channelsNew();
rpChats.channelsEdit();
rpChats.channelsDelete();

rpChats.messages();
rpChats.messagesNew();
rpChats.messagesEdit();
rpChats.messagesDelete();
```


## API explicit version

### Every method within `RPChats` class receives a version as the last argument.

### The default version equals the major version of the package.

```js
import RPChats from 'rp-chat';

const rpChats = new RPChats(config);

rpChats.onChats((chats: Chat[]) => {}, { version: 'v1' });
// ...

rpChats.onChannels((channels: Channel[]) => {}, { version: 'v1' });
// ...

rpChats.onMessages((messages: Message[]) => {}, { version: 'v1' });
// ...

rpChats.chats({ version: 'v1' });
// ...

rpChats.channels({ version: 'v1' });
// ...

rpChats.messages({ version: 'v1' });
// ...

```


## Async API

### Async version waits response in place and returns it within Promise.

```js
await rpChats.messagesAsync();
await rpChats.messagesNewAsync();
await rpChats.messagesEditAsync();
await rpChats.messagesDeleteAsync();
```


## Low level API

```js
import { RPChatsAPI } from 'rp-chat';

const rpChatsAPI = new RPChatsAPI(config);

rpChatsAPI.chats({ version: 'v1' });
rpChatsAPI.chatsNew({ version: 'v1' });
rpChatsAPI.chatsEdit({ version: 'v1' });
rpChatsAPI.chatsDelete({ version: 'v1' });

rpChatsAPI.on('v1/chats', (chats: Chat[]) => {});
rpChatsAPI.on('v1/chats/new', (gchat: GroupChat) => {});
rpChatsAPI.on('v1/chats/edit', (gchat: GroupChat) => {});
rpChatsAPI.on('v1/chats/delete', (gchat: GroupChat) => {});

rpChatsAPI.channels({ version: 'v1' });
rpChatsAPI.channelsNew();
rpChatsAPI.channelsEdit();
rpChatsAPI.channelsDelete();

rpChatsAPI.on('v1/channels', (channels: Channel[]) => {});
rpChatsAPI.on('v1/channels/new', (channel: Channel) => {});
rpChatsAPI.on('v1/channels/edit', (channel: Channel) => {});
rpChatsAPI.on('v1/channels/delete', (channel: Channel) => {});

rpChatsAPI.messages();
rpChatsAPI.messagesNew();
rpChatsAPI.messagesEdit();
rpChatsAPI.messagesDelete();

rpChatsAPI.on('v1/messages', (message: Message) => {});
rpChatsAPI.on('v1/messages/new', (message: Message) => {});
rpChatsAPI.on('v1/messages/edit', (message: Message) => {});
rpChatsAPI.on('v1/messages/delete', (message: Message) => {});
```
