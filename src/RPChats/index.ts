import assert from 'assert';
import { MessageId, Version, v1 } from 'rp-chat-types';
import * as _ from 'lodash';

import { RPChatsAPI } from '../RPChatsAPI';

interface IParams {
    messageId: MessageId
}

type TeardownLogic = () => void;

interface IConfig {
    host: string;
    port: number;
    token: string;
    userId: number;
    transports?: [
        'polling'?,
        'websocket'?
    ]
}

class RPChats {
    static readonly ASYNC_TIMEOUT_MS = 5 * 60 * 1000;
    static readonly ASYNC_TIMEOUT_ERROR = (method: string) => new Error(`RPChats timeout error for ${method}`);

    private readonly _rpChatsAPI: RPChatsAPI;

    constructor(
        private readonly config: IConfig
    ) {
        if (!_.isString(config.host) || _.isEmpty(config.host)) {
            throw new Error('Invalid configuration, host must be not empty string');
        }

        if (!_.isNumber(config.port) || config.port <= 0 || config.port >= 65535) {
            throw new Error('Invalid configuration, port must be valid integer');
        }

        if (!_.isString(config.token) || _.isEmpty(config.token)) {
            throw new Error('Invalid configuration, token must be not empty string');
        }

        if (!_.isNumber(config.userId)) {
            throw new Error('Invalid configuration, userId must be valid integer');
        }

        this._rpChatsAPI = new RPChatsAPI(this.config);
    }

    async init() {
        await this._rpChatsAPI.init();
    }

    async deinit() {
        await this._rpChatsAPI.deinit();
    }

    onError(cb: (error: Error, params?: IParams) => void): TeardownLogic {
        const handler = (error: Error, params?: IParams) => cb(error);
        const internalErrorHandler = (error: string, params: IParams) => cb(new Error(error), params);

        this._rpChatsAPI.on('error', handler);
        this._rpChatsAPI.on('v1/internal-error', internalErrorHandler);

        return () => {
            this._rpChatsAPI.removeListener('error', handler);
            this._rpChatsAPI.removeListener('v1/internal-error', internalErrorHandler);
        };
    }

    chats(params?: { version: Version }) {
        return this._rpChatsAPI.chats(params);
    }

    chatsDescribe(chatId: number, params?: { version: Version }) {
        return this._rpChatsAPI.chatsDescribe(chatId, params);
    }

    chatsMembers(chatId: number, params?: { version: Version }) {
        return this._rpChatsAPI.chatsMembers(chatId, params);
    }

    chatsMembersEdit(chatId: number, membersToAdd: number[], membersToRemove: number[], params?: { version: Version }) {
        return this._rpChatsAPI.chatsMembersEdit(chatId, membersToAdd, membersToRemove, params);
    }

    chatsNew(newChat: v1.INewChat, params?: { version: Version }) {
        return this._rpChatsAPI.chatsNew(newChat, params);
    }

    chatsEdit(editChat: v1.IEditChat, params?: { version: Version }) {
        return this._rpChatsAPI.chatsEdit(editChat, params);
    }

    chatsDelete(deleteChat: v1.IDeleteChat, params?: { version: Version }) {
        return this._rpChatsAPI.chatsDelete(deleteChat, params);
    }

    channels(params?: { version: Version }) {
        return this._rpChatsAPI.channels(params);
    }

    channelsNew(newChannel: v1.INewChannel, params?: { version: Version }) {
        return this._rpChatsAPI.channelsNew(newChannel, params);
    }

    channelsDescribe(channelId: number, params?: { version: Version }) {
        return this._rpChatsAPI.channelsDescribe(channelId, params);
    }

    channelsMembers(channelId: number, params?: { version: Version }) {
        return this._rpChatsAPI.channelsMembers(channelId, params);
    }

    channelsMembersEdit(channelId: number, membersToAdd: number[], membersToRemove: number[], params?: { version: Version }) {
        return this._rpChatsAPI.channelsMembersEdit(channelId, membersToAdd, membersToRemove, params);
    }

    channelsEdit(editChannel: v1.IEditChannel, params?: { version: Version }) {
        return this._rpChatsAPI.channelsEdit(editChannel, params);
    }

    channelsDelete(deleteChannel: v1.IDeleteChannel, params?: { version: Version }) {
        return this._rpChatsAPI.channelsDelete(deleteChannel, params);
    }

    channelsRead(request: v1.IReadChannel, params?: { version: Version }) {
        return this._rpChatsAPI.channelsRead(request, params);
    }

    channelsUnread(params?: { version: Version }) {
        return this._rpChatsAPI.channelsUnread(params);
    }

    channelsLeave(leaveChannel: v1.ILeaveChannel, params?: { version: Version }) {
        return this._rpChatsAPI.channelsLeave(leaveChannel, params);
    }

    messages(request: { channelId: number, page: number }, params?: { version: Version }) {
        return this._rpChatsAPI.messages(request);
    }

    messagesNew(newMessage: v1.INewMessage, params?: { version: Version }) {
        return this._rpChatsAPI.messagesNew(newMessage, params);
    }

    messagesEdit(editMessage: v1.IEditMessage, params?: { version: Version }) {
        return this._rpChatsAPI.messagesEdit(editMessage, params);
    }

    messagesDelete(deleteMessage: v1.IDeleteMessage, params?: { version: Version }) {
        return this._rpChatsAPI.messagesDelete(deleteMessage, params);
    }

    onChats(cb: (chats: v1.IChat[], params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (chats: v1.IChat[], params: IParams) => cb(chats, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChatsDescribe(cb: (chat: v1.IChatDetails, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (chat: v1.IChatDetails, params: IParams) => cb(chat, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats/describe', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats/describe', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChatsMembers(cb: (members: v1.IChatMember[], chatId: number, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (members: v1.IChatMember[], chatId: number, params: IParams) => cb(members, chatId, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats/members', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats/members', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChatsNew(cb: (chat: v1.IChat, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (chat: v1.IChat, params: IParams) => cb(chat, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats/new', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats/new', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChatsEdit(cb: (chat: v1.IChat, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (chat: v1.IChat, params: IParams) => cb(chat, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats/edit', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats/edit', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChatsDelete(cb: (chat: v1.IChat, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (chat: v1.IChat, params: IParams) => cb(chat, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/chats/delete', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/chats/delete', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannels(cb: (channels: v1.IChannel[], params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channels: v1.IChannel[], params: IParams) => cb(channels, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsDescribe(cb: (channel: v1.IChannelDetails, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channel: v1.IChannelDetails, params: IParams) => cb(channel, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/describe', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/describe', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsMembers(cb: (members: v1.IChannelMember[], channelId: number, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (members: v1.IChannelMember[], channelId: number, params: IParams) => cb(members, channelId, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/members', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/members', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsNew(cb: (channel: v1.IChannel, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channel: v1.IChannel, params: IParams) => cb(channel, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/new', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/new', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsEdit(cb: (channel: v1.IChannel, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channel: v1.IChannel, params: IParams) => cb(channel, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/edit', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/edit', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsDelete(cb: (channel: v1.IChannel, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channel: v1.IChannel, params: IParams) => cb(channel, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/delete', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/delete', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsRead(cb: (response: v1.IReadChannel, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (response: v1.IReadChannel, params: IParams) => cb(response, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/read', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/read', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsUnread(cb: (response: v1.IUnreadChannel[], params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (response: v1.IUnreadChannel[], params: IParams) => cb(response, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/unread', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/unread', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onChannelsLeave(cb: (channel: v1.IChannel, userId: number, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (channel: v1.IChannel, userId: number, params: IParams) => cb(channel, userId, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/channels/leave', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/channels/leave', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onMessages(cb: (messages: v1.IMessage[], page: number, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (messages: v1.IMessage[], page: number, params: IParams) => cb(messages, page, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/messages', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/messages', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onMessagesNew(cb: (messages: v1.IMessage, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (message: v1.IMessage, params: IParams) => cb(message, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/messages/new', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/messages/new', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onMessagesEdit(cb: (messages: v1.IMessage, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (message: v1.IMessage, params: IParams) => cb(message, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/messages/edit', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/messages/edit', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    onMessagesDelete(cb: (messages: v1.IMessage, params: IParams) => void, params: { version: Version } = { version: 'v1' }): TeardownLogic {
        const handler = (message: v1.IMessage, params: IParams) => cb(message, params);

        switch (params.version) {
            case 'v1':
                this._rpChatsAPI.on('v1/messages/delete', handler);

                return () => {
                    this._rpChatsAPI.removeListener('v1/messages/delete', handler);
                };
            default:
                assert(false, 'Invalid version');

                return null;
        }
    }

    chatsAsync(params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChat[]>((resolve, reject) => {
            const messageId = this.chats(params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsAsync'));
            }, params.timeout || 5000);

            const cancelFunc = this.onChats((chats, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(chats);
                }
            }, params);
        });
    }

    chatsDescribeAsync(chatId: number, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChatDetails>((resolve, reject) => {
            const messageId = this.chatsDescribe(chatId);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsDescribeAsync'));
            }, params.timeout);

            const cancelFunc = this.onChatsDescribe((chat, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(chat);
                }
            }, params);
        });
    }

    chatsMembersAsync(chatId: number, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<{ chatId: number, members: v1.IChannelMember[] }>((resolve, reject) => {
            const messageId = this.chatsMembers(chatId);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsMembersAsync'));
            }, params.timeout);

            const cancelFunc = this.onChatsMembers((members, chatId, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve({ chatId, members });
                }
            }, params);
        });
    }

    chatsNewAsync(newChat: v1.INewChat, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChat>((resolve, reject) => {
            const messageId = this.chatsNew(newChat, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsNewAsync'));
            }, params.timeout);

            const cancelFunc = this.onChatsNew((chat, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(chat);
                }
            }, params);
        });
    }

    chatsEditAsync(editChat: v1.IEditChat, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChat>((resolve, reject) => {
            const messageId = this.chatsEdit(editChat, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsEditAsync'));
            }, params.timeout);

            const cancelFunc = this.onChatsEdit((chat, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(chat);
                }
            }, params);
        });
    }

    chatsDeleteAsync(deleteChat: v1.IDeleteChat, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChat>((resolve, reject) => {
            const messageId = this.chatsDelete(deleteChat, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsDeleteAsync'));
            }, params.timeout);

            const cancelFunc = this.onChatsDelete((chat, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(chat);
                }
            }, params);
        });
    }

    channelsAsync(params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChannel[]>((resolve, reject) => {
            const messageId = this.channels(params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannels((channels, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(channels);
                }
            }, params);
        });
    }

    channelsDescribeAsync(channelId: number, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChannelDetails>((resolve, reject) => {
            const messageId = this.channelsDescribe(channelId);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsDescribeAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsDescribe((channel, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(channel);
                }
            }, params);
        });
    }

    channelsMembersAsync(channelId: number, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<{ channelId: number, members: v1.IChannelMember[] }>((resolve, reject) => {
            const messageId = this.channelsMembers(channelId);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsMembersAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsMembers((members, channelId, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve({ channelId, members });
                }
            }, params);
        });
    }

    channelsNewAsync(newChannel: v1.INewChannel, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChannel>((resolve, reject) => {
            const messageId = this.channelsNew(newChannel, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsNewAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsNew((channel, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(channel);
                }
            }, params);
        });
    }

    channelsEditAsync(editChannel: v1.IEditChannel, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChannel>((resolve, reject) => {
            const messageId = this.channelsEdit(editChannel, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsEditAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsEdit((channel, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(channel);
                }
            }, params);
        });
    }

    channelsDeleteAsync(deleteChannel: v1.IDeleteChannel, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IChannel>((resolve, reject) => {
            const messageId = this.channelsDelete(deleteChannel, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsDeleteAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsDelete((channel, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(channel);
                }
            }, params);
        });
    }

    channelsReadAsync(request: v1.IReadChannel, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IReadChannel>((resolve, reject) => {
            const messageId = this.channelsRead(request, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsReadAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsRead((response, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(response);
                }
            }, params);
        });
    }

    channelsUnreadAsync(params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IUnreadChannel[]>((resolve, reject) => {
            const messageId = this.channelsUnread(params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('chatsCountersAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsUnread((response, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(response);
                }
            }, params);
        });
    }

    channelsLeaveAsync(leaveChannel: v1.ILeaveChannel, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<{ channel: v1.IChannel, userId: number }>((resolve, reject) => {
            const messageId = this.channelsLeave(leaveChannel, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('channelsLeaveAsync'));
            }, params.timeout);

            const cancelFunc = this.onChannelsLeave((channel, userId, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve({ channel, userId });
                }
            }, params);
        });
    }

    messagesAsync(request: { channelId: number, page: number }, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<{ messages: v1.IMessage[], page: number }>((resolve, reject) => {
            const messageId = this.messages(request, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('messagesAsync'));
            }, params.timeout);

            const cancelFunc = this.onMessages((messages, page, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve({ messages, page });
                }
            }, params);
        });
    }

    messagesNewAsync(newMessage: v1.INewMessage, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IMessage>((resolve, reject) => {
            const messageId = this.messagesNew(newMessage, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('messagesNewAsync'));
            }, params.timeout);

            const cancelFunc = this.onMessagesNew((message, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(message);
                }
            }, params);
        });
    }

    messagesEditAsync(editMessage: v1.IEditMessage, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IMessage>((resolve, reject) => {
            const messageId = this.messagesEdit(editMessage, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('messagesEditAsync'));
            }, params.timeout);

            const cancelFunc = this.onMessagesEdit((message, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(message);
                }
            }, params);
        });
    }

    messagesDeleteAsync(deleteMessage: v1.IDeleteMessage, params: { version: Version, timeout?: number } = { version: 'v1', timeout: RPChats.ASYNC_TIMEOUT_MS }) {
        return new Promise<v1.IMessage>((resolve, reject) => {
            const messageId = this.messagesDelete(deleteMessage, params);

            const timeout = setTimeout(() => {
                cancelFunc();
                return reject(RPChats.ASYNC_TIMEOUT_ERROR('messagesDeleteAsync'));
            }, params.timeout);

            const cancelFunc = this.onMessagesDelete((message, params) => {
                if (params.messageId === messageId) {
                    clearTimeout(timeout);

                    return resolve(message);
                }
            }, params);
        });
    }
}

export { RPChats };
