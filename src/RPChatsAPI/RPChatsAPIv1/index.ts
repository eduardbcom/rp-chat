import { EventEmitter } from 'eventemitter3';
import io, { Socket } from 'socket.io-client';
import { v4 as uuid } from 'uuid';

import { MessageId, v1 } from 'rp-chat-types';

interface Params {
    messageId: MessageId
}

interface ResponseMap {
    'error': (error: Error) => void;

    'initialized': () => void;
    'internal-error': (error: string, params: Params) => boolean;

    'chats': (chats: v1.IChat[], params: Params) => boolean;
    'chats/describe': (chat: v1.IChatDetails, params: Params) => boolean;
    'chats/members': (members: v1.IChatMember[], chatId: number, params: Params) => boolean;
    'chats/new': (chat: v1.IChat, params: Params) => boolean;
    'chats/edit': (chat: v1.IChat, params: Params) => boolean;
    'chats/delete': (chat: v1.IChat, params: Params) => boolean;

    'channels': (channels: v1.IChannel[], params: Params) => boolean;
    'channels/describe': (channel: v1.IChannelDetails, params: Params) => boolean;
    'channels/members': (members: v1.IChannelMember[], chatId: number, params: Params) => boolean;
    'channels/new': (channel: v1.IChannel, params: Params) => boolean;
    'channels/edit': (channel: v1.IChannel, params: Params) => boolean;
    'channels/delete': (channel: v1.IChannel, params: Params) => boolean;
    'channels/read': (response: v1.IReadChannel, params: Params) => boolean;
    'channels/unread': (response: v1.IUnreadChannel[], params: Params) => boolean;
    'channels/leave': (response: v1.IChannel, userId: number, params: Params) => boolean;

    'messages': (messages: v1.IMessage[], page: number, params: Params) => boolean;
    'messages/new': (message: v1.IMessage, params: Params) => boolean;
    'messages/edit': (message: v1.IMessage, params: Params) => boolean;
    'messages/delete': (message: v1.IMessage, params: Params) => boolean;
}

interface RequestMap {
    'internal-error': (error: string, params: Params) => void;

    'chats': (params: Params) => void;
    'chats/describe': (chatId: number, params: Params) => void;
    'chats/members': (chatId: number, params: Params) => void;
    'chats/members/edit': (chatId: number, membersToAdd: number[], membersToRemove: number[], params: Params) => boolean;
    'chats/new': (chat: v1.INewChat, params: Params) => void;
    'chats/edit': (chat: v1.IEditChat, params: Params) => void;
    'chats/delete': (chat: v1.IDeleteChat, params: Params) => void;

    'channels': (params: Params) => void;
    'channels/describe': (channelId: number, params: Params) => void;
    'channels/members': (channelId: number, params: Params) => void;
    'channels/members/edit': (channelId: number, membersToAdd: number[], membersToRemove: number[], params: Params) => boolean;
    'channels/new': (channel: v1.INewChannel, params: Params) => void;
    'channels/edit': (channel: v1.IEditChannel, params: Params) => void;
    'channels/delete': (channel: v1.IDeleteChannel, params: Params) => void;
    'channels/read': (request: v1.IReadChannel, params: Params) => void;
    'channels/unread': (params: Params) => void;
    'channels/leave': (channel: v1.ILeaveChannel, params: Params) => void;

    'messages': (request: { channelId: number, page: number }, params: Params) => void;
    'messages/new': (message: v1.INewMessage, params: Params) => void;
    'messages/edit': (message: v1.IEditMessage, params: Params) => void;
    'messages/delete': (message: v1.IDeleteMessage, params: Params) => void;
}

interface IConfig {
    host: string;
    port: number;
    namespace: string;
    token: string;
    userId: number;
    transports?: [
        'polling'?,
        'websocket'?
    ]
}

class RPChatsAPIv1 extends EventEmitter<ResponseMap, RequestMap> {
    private readonly _CONNECT_TIMEOUT_MS = 15 * 1000;
    private readonly _DISCONNECT_TIMEOUT_MS = 15 * 1000;

    private _socket: Socket<ResponseMap, RequestMap>;

    constructor(
        private readonly config: IConfig
    ) {
        super();
    }

    async init() {
        if (this._socket) {
            throw new Error('Already initialized');
        }

        this._socket = io(
            this._generateConnectionURL(),
            { autoConnect: false, query: { token: this.config.token, user_id: this.config.userId + "" }, transports: this.config.transports || ['polling', 'websocket'], rejectUnauthorized: false }
        );

        this._socket.on('internal-error', (error: string, params: Params) => this.emit('internal-error', error, params));

        this._socket.on('chats', (chats: v1.IChat[], params: Params) => this.emit('chats', chats, params));
        this._socket.on('chats/describe', (chat: v1.IChatDetails, params: Params) => this.emit('chats/describe', chat, params));
        this._socket.on('chats/members', (members: v1.IChatMember[], chatId: number, params: Params) => this.emit('chats/members', members, chatId, params));
        this._socket.on('chats/new', (chat: v1.IChat, params: Params) => this.emit('chats/new', chat, params));
        this._socket.on('chats/edit', (chat: v1.IChat, params: Params) => this.emit('chats/edit', chat, params));
        this._socket.on('chats/delete', (chat: v1.IChat, params: Params) => this.emit('chats/delete', chat, params));

        this._socket.on('channels', (channels: v1.IChannel[], params: Params) => this.emit('channels', channels, params));
        this._socket.on('channels/describe', (channel: v1.IChannelDetails, params: Params) => this.emit('channels/describe', channel, params));
        this._socket.on('channels/members', (members: v1.IChannelMember[], channelId: number, params: Params) => this.emit('channels/members', members, channelId, params));
        this._socket.on('channels/new', (channel: v1.IChannel, params: Params) => this.emit('channels/new', channel, params));
        this._socket.on('channels/edit', (channel: v1.IChannel, params: Params) => this.emit('channels/edit', channel, params));
        this._socket.on('channels/delete', (channel: v1.IChannel, params: Params) => this.emit('channels/delete', channel, params));
        this._socket.on('channels/read', (response: v1.IReadChannel, params: Params) => this.emit('channels/read', response, params));
        this._socket.on('channels/unread', (response: v1.IUnreadChannel[], params: Params) => this.emit('channels/unread', response, params));
        this._socket.on('channels/leave', (channel: v1.IChannel, userId: number, params: Params) => this.emit('channels/leave', channel, userId, params));

        this._socket.on('messages', (messages: v1.IMessage[], page: number, params: Params) => this.emit('messages', messages, page, params));
        this._socket.on('messages/new', (message: v1.IMessage, params: Params) => this.emit('messages/new', message, params));
        this._socket.on('messages/edit', (message: v1.IMessage, params: Params) => this.emit('messages/edit', message, params));
        this._socket.on('messages/delete', (message: v1.IMessage, params: Params) => this.emit('messages/delete', message, params));

        return new Promise<void>((resolve, reject) => {
            const timeout = setTimeout(() => {
                this._socket.off();
                this._socket = null;

                return reject(new Error('Connection timeout'));
            }, this._CONNECT_TIMEOUT_MS);

            this._socket.once('initialized', () => {
                this._socket.off('connect_error');
                this._socket.off('error');

                this._socket.on('connect_error', this._onError);
                this._socket.on('error', this._onError);
                this._socket.on('disconnect', this._onDisconnect);

                clearTimeout(timeout);

                return resolve();
            });

            this._socket.once('connect_error', (error: Error) => {
                this._socket.off();
                this._socket = null;

                clearTimeout(timeout);

                return reject(error);
            });

            this._socket.once('error', (error: Error) => {
                this._socket.off();
                this._socket = null;

                clearTimeout(timeout);

                return reject(error);
            });

            this._socket.connect();
        });
    }

    async deinit() {
        if (!this._socket) {
            throw new Error('Already deinitialized');
        }

        return new Promise<void>((resolve, reject) => {
            const timeout = setTimeout(() => {
                this._socket.off();
                this._socket = null;

                return reject(new Error('Disconnection timeout'));
            }, this._DISCONNECT_TIMEOUT_MS);

            this._socket.off('connect_error', this._onError);
            this._socket.off('error', this._onError);
            this._socket.off('disconnect', this._onDisconnect);

            this._socket.once('disconnect', () => {
                this._socket.off();
                this._socket = null;

                clearTimeout(timeout);

                return resolve();
            });

            this._socket.once('error', (error: Error) => {
                this._socket.off();
                this._socket = null;

                clearTimeout(timeout);

                return reject(error);
            });

            this._socket.disconnect();
        });
    }

    chats(): MessageId {
        const messageId = uuid();

        console.log('chats', messageId, this._socket.id, this._socket.connected);
        this._socket.emit('chats', { messageId });

        return messageId;
    }

    chatsDescribe(chatId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/describe', chatId, { messageId });

        return messageId;
    }

    chatsMembers(chatId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/members', chatId, { messageId });

        return messageId;
    }

    chatsMembersEdit(chatId: number, membersToAdd: number[], membersToRemove: number[]): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/members/edit', chatId, membersToAdd, membersToRemove, { messageId });

        return messageId;
    }

    chatsNew(newChat: v1.INewChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/new', newChat, { messageId });

        return messageId;
    }

    chatsEdit(editChat: v1.IEditChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/edit', editChat, { messageId });

        return messageId;
    }

    chatsDelete(deleteChat: v1.IDeleteChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/delete', deleteChat, { messageId });

        return messageId;
    }

    channels(): MessageId {
        const messageId = uuid();

        this._socket.emit('channels', { messageId });

        return messageId;
    }

    channelsDescribe(channelId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/describe', channelId, { messageId });

        return messageId;
    }

    channelsMembers(channelId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/members', channelId, { messageId });

        return messageId;
    }

    channelsMembersEdit(channelId: number, membersToAdd: number[], membersToRemove: number[]): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/members/edit', channelId, membersToAdd, membersToRemove, { messageId });

        return messageId;
    }

    channelsNew(newChannel: v1.INewChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/new', newChannel, { messageId });

        return messageId;
    }

    channelsEdit(editChannel: v1.IEditChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/edit', editChannel, { messageId });

        return messageId;
    }

    channelsDelete(deleteChannel: v1.IDeleteChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/delete', deleteChannel, { messageId });

        return messageId;
    }

    channelsRead(request: v1.IReadChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/read', request, { messageId });

        return messageId;
    }

    channelsUnread(): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/unread', { messageId });

        return messageId;
    }

    channelsLeave(request: v1.ILeaveChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/leave', request, { messageId });

        return messageId;
    }

    messages(request: { channelId: number, page: number }): MessageId {
        const messageId = uuid();

        this._socket.emit('messages', request, { messageId });

        return messageId;
    }

    messagesNew(newMessage: v1.INewMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/new', newMessage, { messageId });

        return messageId;
    }

    messagesEdit(editMessage: v1.IEditMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/edit', editMessage, { messageId });

        return messageId;
    }

    messagesDelete(deleteMessage: v1.IDeleteMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/delete', deleteMessage, { messageId });

        return messageId;
    }

    private _onError = (error: Error) => {
        this.emit('error', error);
    }

    private _onDisconnect = (error: string) => {
        // https://socket.io/docs/v3/client-api/index.html#Event-%E2%80%98disconnect%E2%80%99
        const reconnectableErrors = ["io server disconnect"];

        if (reconnectableErrors.find(_ => _ === error)) {
            return this._socket.connect();
        }
    };

    private _generateConnectionURL() {
        return `${this.config.host}:${this.config.port}/${this.config.namespace}`;
    }
}

export { RPChatsAPIv1 };
