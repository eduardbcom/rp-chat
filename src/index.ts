import { RPChats } from './RPChats';
import { RPChatsAPI } from './RPChatsAPI';

export { RPChatsAPI };
export default RPChats;
